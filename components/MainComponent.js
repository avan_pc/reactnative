import React, { Component } from 'react';
import Menu from './MenuComponent';
import Home from './HomeComponent';
import Dishdetail from "./DishdetailComponent";
import contactUs from './ContactComponent';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator, DrawerContentScrollView , DrawerItemList, DrawerItem} from '@react-navigation/drawer'
import AboutUs from "./AboutComponent";
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import { Platform,View,Text,Image, StyleSheet, SafeAreaView} from 'react-native';
import {fetchDishes, fetchComments, fetchLeaders, fetchPromos} from "../redux/ActionCreators";
import Reservation from './ReservationComponent';
import Favorites from './FavouriteComponent'

const mapStateToProps = state => {
    return {
    }
};

const mapDispatchToProps = dispatch => ({
    fetchDishes: () => dispatch(fetchDishes()),
    fetchComments: () => dispatch(fetchComments()),
    fetchPromos: () => dispatch(fetchPromos()),
    fetchLeaders: () => dispatch(fetchLeaders()),
});


const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const blueTheme = {
    dark: false,
    colors: {
        primary: 'rgb(0, 0, 0)',
        background: '#D1C4E9',
        card: '#512DA8',
        text: 'rgb(0,0,0)',
        border: 'rgb(199, 199, 204)',
        notification: 'rgb(255, 69, 58)',
    },
};

function CustomDrawerContentComponent(props) {
    return(
        <DrawerContentScrollView {...props}>
            <SafeAreaView style={styles.container}
                forceInset={{top: 'always', horizontal: 'never'}}>
                <View style={styles.drawerHeader}>
                    <View style={{flex:1}}>
                        <Image source={require('./images/logo.png')}
                               style={styles.drawerImage} />
                    </View>
                    <View style={{flex:2}}>
                        <Text style={styles.drawerHeaderText}>
                            Ristorante Con Fusion
                        </Text>
                    </View>
                </View>
                <DrawerItemList {...props} />
            </SafeAreaView>
        </DrawerContentScrollView>
    );
}

function MenuNavigator(props) {
    return(
            <Stack.Navigator>
                <Stack.Screen name="Menu" component={Menu} options={{ headerStyle:{
                        backgroundColor: '#512da8'
                    },
                    headerTintColor: '#fff',
                    headerTitleStyle: {
                        color: '#fff'
                    },
                    headerLeft: () => (<Icon color='white' size={24} onPress={() => props.navigation.toggleDrawer()} name={Platform.OS === 'android'? 'md-list': 'ios-list' }> </Icon>)
                }} />
                <Stack.Screen name="Dishdetail" component={Dishdetail} options={{ headerStyle:{
                        backgroundColor: '#512da8'
                    },
                    headerTintColor: '#fff',
                    headerTitleStyle: {
                        color: '#fff'
                    }}} />
                <Stack.Screen name="Home" component={Home} />
                <Stack.Screen name={"Contact Us"} component={contactUs} />
            </Stack.Navigator>
    );
}
function HomeNavigator (props) {
    return(
        <Stack.Navigator initialRouteName="Home">
            <Stack.Screen name="Home" component={Home} options={{ headerStyle:{
                backgroundColor: '#512da8'
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                color: '#fff'
                },
                headerLeft: () => (<Icon color='white' onPress={() => props.navigation.toggleDrawer()} size={24} name={Platform.OS === 'android'? 'md-menu': 'ios-menu' }> </Icon>)
            }} />
        </Stack.Navigator>
    );
}
function ContactNavigator (props) {
    return(
        <Stack.Navigator>
            <Stack.Screen name=" " component={contactUs} options={{ headerStyle:{
                    backgroundColor: '#512da8'
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                    color: '#fff'
                },
                headerLeft: () => (<Icon color='white' onPress={() => props.navigation.toggleDrawer()} size={24} name={Platform.OS === 'android'? 'md-menu': 'ios-menu' }> </Icon>)
            }} />
        </Stack.Navigator>
    );
}

function ReservationNavigator (props) {
    return(
        <Stack.Navigator>
            <Stack.Screen name="Reservation" component={Reservation} options={{ headerStyle:{
                    backgroundColor: '#512da8'
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                    color: '#fff'
                },
                headerLeft: () => (<Icon color='white' onPress={() => props.navigation.toggleDrawer()} size={24} name={Platform.OS === 'android'? 'md-menu': 'ios-menu' }> </Icon>)
            }} />
        </Stack.Navigator>
    );
}

function AboutNavigator (props) {
    return(
        <Stack.Navigator>
            <Stack.Screen name="About Us" component={AboutUs} options={{ headerStyle:{
                    backgroundColor: '#512da8'
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                    color: '#fff'
                },
                headerLeft: () => (<Icon color='white' onPress={() => props.navigation.toggleDrawer()} size={24} name={Platform.OS === 'android'? 'md-menu': 'ios-menu' }> </Icon>)
            }} />
        </Stack.Navigator>
    );
}

function FavoritesNavigator (props) {
    return(
        <Stack.Navigator>
            <Stack.Screen name="Favorites" component={Favorites} options={{ headerStyle:{
                    backgroundColor: '#512da8'
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                    color: '#fff'
                },
                headerLeft: () => (<Icon color='white' onPress={() => props.navigation.toggleDrawer()} size={24} name={Platform.OS === 'android'? 'md-menu': 'ios-menu' }> </Icon>)
            }} />
        </Stack.Navigator>
    );
}
class Main extends Component {
    componentDidMount() {
        this.props.fetchDishes();
        this.props.fetchComments();
        this.props.fetchPromos();
        this.props.fetchLeaders();
    }

  render() {
    return (
        <NavigationContainer theme={blueTheme}>
            <Drawer.Navigator initialRouteName="Home" drawerStyle={{
                backgroundColor: '#D1C4E9'}} drawerContent={(props) => <CustomDrawerContentComponent {...props} />}>
                <Drawer.Screen name="Home" component={HomeNavigator} options={{
                    drawerIcon: config => <Icon size={24} color='rgb(0,0,0)' name={Platform.OS === 'android' ? 'md-home' : 'ios-home'}> </Icon>
                }} />
                <Drawer.Screen name="About Us" component={AboutNavigator}  options={{
                    drawerIcon: config => <Icon
                        name={Platform.OS === 'android' ? 'md-information-circle' : 'ios-information-circle'} size={24}
                        color= 'rgb(0,0,0)'
                    > </Icon>
                }} />
                <Drawer.Screen name="Menu" component={MenuNavigator}
                    options={{
                        drawerIcon: config => <Icon
                            name={Platform.OS === 'android' ? 'md-list' : 'ios-list'} size={24}
                            color= 'rgb(0,0,0)'
                        > </Icon>
                    }} />
                <Drawer.Screen name="Contact Us" component={ContactNavigator} options={{
                    drawerIcon: config => <Icon
                    name={Platform.OS === 'android' ? 'md-call' : 'ios-call'} size={24}
                    color= 'rgb(0,0,0)'
                > </Icon> }} />
                <Drawer.Screen name="My Favourites" component={FavoritesNavigator} options={{
                    drawerIcon: config => <Icon
                        name='heart' size={24}
                        color= 'rgb(0,0,0)'
                    > </Icon> }} />
                <Drawer.Screen name="Reserve Table" component={ReservationNavigator} options={{
                    drawerIcon: config => <Icon
                        name='restaurant' size={24}
                        color= 'rgb(0,0,0)'
                    > </Icon> }} />
            </Drawer.Navigator>
        </NavigationContainer>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    drawerHeader: {
        backgroundColor: '#512DA8',
        height: 140,
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'row'
    },
    drawerHeaderText: {
        color: 'white',
        fontSize: 24,
        fontWeight: 'bold'
    },
    drawerImage: {
        margin: 10,
        width: 80,
        height: 60
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);