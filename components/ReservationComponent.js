import React, { Component, useState} from 'react';
import { Text, View, ScrollView, StyleSheet, Picker, Switch, Button, Modal, Alert } from 'react-native';
import { Card } from 'react-native-elements';
import DateTimePicker from '@react-native-community/datetimepicker';
import * as Animatable from "react-native-animatable";

function Reservation() {
    function handleReservation() {
        Alert.alert("Your Reservation OK?", "Number of Guests: " + JSON.stringify(guests) +'\n'
        + "Smoking? "+ smoking +'\n'+ "Date and Time: "+ JSON.stringify(date),[{text: 'Cancel'},
            {text:"OK", onPress: () => resetForm()}], {cancelable: false}
        );
    }

    const [guests, setGuests]  = useState(1);
    const [smoking, setSmoking] = useState(false);
    const [date, setDate] = useState(new Date(1598051730000));
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);
    const [showModal, setShowModal] = useState(false);

    const toggleModal = previous => !previous;

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
    };

    const showMode = (currentMode) => {
        setShow(true);
        setMode(currentMode);
    };

    const showDatepicker = () => {
        showMode('date');
    };

    const showTimepicker = () => {
        showMode('time');
    };

    const resetForm=()=> {
        setDate(1598051730000);
        setSmoking(false);
        setGuests(1);
    };

    return (
        <ScrollView>
            <Animatable.View animation="zoomIn" duration={2000}>
            <View style={styles.formRow}>
                <Text style={styles.formLabel}>Number of Guests</Text>
                <Picker
                    style={styles.formItem}
                    selectedValue={guests}
                    onValueChange={(itemValue, itemIndex) => setGuests((itemValue))}>
                    <Picker.Item label="1" value="1"/>
                    <Picker.Item label="2" value="2"/>
                    <Picker.Item label="3" value="3"/>
                    <Picker.Item label="4" value="4"/>
                    <Picker.Item label="5" value="5"/>
                    <Picker.Item label="6" value="6"/>
                </Picker>
            </View>
            <View style={styles.formRow}>
                <Text style={styles.formLabel}>Smoking/Non-Smoking?</Text>
                <Switch
                    style={styles.formItem}
                    value={smoking}
                    onTintColor='#512DA8'
                    onValueChange={(value) => setSmoking((value))}>
                </Switch>
            </View>

            <View style={styles.formRow}>
                <Text style={styles.formLabel}> Date & Time</Text>
                <View>
                    <View style={styles.formRow}>
                        <Button onPress={showDatepicker} title="Date"/>
                        <Button onPress={showTimepicker} title="Time"/>
                    </View>
                    {show && (
                        <DateTimePicker
                            testID="dateTimePicker"
                            value={date}
                            mode={mode}
                            is24Hour={true}
                            display="default"
                            onChange={onChange}
                        />
                    )}
            </View>
            </View>
            <View style={styles.formRow}>
                <Button
                    title="Reserve"
                    color="#512DA8"
                    onPress={() => handleReservation()}
                    accessibilityLabel="Learn more about this purple button"
                />
            </View>
            </Animatable.View>
            <Modal animationType = {"slide"} transparent = {false}
                   visible = {showModal}
                   onDismiss = {() => setShowModal(toggleModal) }
                   onRequestClose = {() => setShowModal(toggleModal) }>
                <View style = {styles.modal}>
                    <Text style = {styles.modalTitle}>Your Reservation</Text>
                    <Text style = {styles.modalText}>Number of Guests: {JSON.stringify(guests)}</Text>
                    <Text style = {styles.modalText}>Smoking?: {smoking ? 'Yes' : 'No'}</Text>
                    <Text style = {styles.modalText}>Date and Time: {JSON.stringify(date)} </Text>

                    <Button
                        onPress = {() =>{setShowModal(toggleModal); resetForm();}}
                        color="#512DA8"
                        title="Close"
                    />
                </View>
            </Modal>
        </ScrollView>

    );
}
const styles = StyleSheet.create({
    formRow: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'row',
        margin: 20
    },
    formLabel: {
        fontSize: 18,
        flex: 2
    },
    formItem: {
        flex: 1
    },
    modal: {
        justifyContent: 'center',
        margin: 20
    },
    modalTitle:{
        fontSize: 24,
        fontWeight: 'bold',
        backgroundColor: '#512DA8',
        textAlign: 'center',
        color: 'white'
    },
    modalText: {
        fontSize: 18,
        margin: 10
    }
});

export default Reservation;