import React, {Component} from "react";
import {FlatList, View, Text, Alert} from 'react-native';
import {ListItem} from 'react-native-elements';
import { Avatar } from 'react-native-elements/dist/avatar/Avatar';
import {connect} from 'react-redux';
import {Loading} from './LoadingComponent';
import {baseUrl} from "../shared/baseUrl";
import Swipeable from "react-native-gesture-handler/Swipeable";
import {deleteFavorite} from "../redux/ActionCreators";
import * as Animatable from 'react-native-animatable';

const mapStateToProps = state => {
    return {
        dishes: state.dishes,
        favorites: state.favorites
    }
};
const mapDispatchToProps = dispatch => ({
    deleteFavorite: (dishId) => dispatch(deleteFavorite(dishId))
});

class Favorites extends Component {

    static navigationOptions = {
        title: 'My Favorites'
    };

    render() {

        const { navigate } = this.props.navigation;

        const renderMenuItem = ({item, index}) => {
            const rightSwipeActions = () => {
                return (
                    <View style={{backgroundColor:"red"}}
                        onStartShouldSetResponder={()=>{Alert.alert(
                          'Delete Favorite',
                          "Are you sure you wish to delete " + item.name + " from your favorites?",
                            [
                                {text: 'Cancel', onPress: ()=> console.log(item.name+ " not deleted"), style:'cancel'},
                                {text: 'OK', onPress: ()=>this.props.deleteFavorite(item.id)}
                            ],
                        {cancelable:false}
                        );}}>
                        <Text style={{color:"white", flex:1,
                            textAlignVertical: "center",
                            paddingHorizontal:35}}>
                            Delete
                        </Text>
                    </View>
                );
            };
            return (

                <Swipeable
                    renderRightActions={rightSwipeActions}>
                    <Animatable.View animation="fadeInRightBig" duration={2000}>
                        <ListItem bottomDivider onPress={() => navigate('Dishdetail', { dishId: item.id })} key={index}>
                            <Avatar
                                title={item.name}
                                source={{uri: baseUrl + item.image}}/>
                            <ListItem.Content>
                                <ListItem.Title style={{fontWeight: "bold"}}>
                                    {item.name}
                                </ListItem.Title>
                                <ListItem.Subtitle>{item.description}</ListItem.Subtitle>
                            </ListItem.Content>
                        </ListItem>
                    </Animatable.View>
                </Swipeable>
            );
        };

        if (this.props.dishes.isLoading) {
            return(
                <Loading />
            );
        }
        else if (this.props.dishes.errMess) {
            return(
                <View>
                    <Text>{this.props.dishes.errMess}</Text>
                </View>
            );
        }
        else {
            return (
                <FlatList
                    data={this.props.dishes.dishes.filter(dish => this.props.favorites.some(el => el === dish.id))}
                    renderItem={renderMenuItem}
                    keyExtractor={item => item.id.toString()}
                />
            );
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Favorites);