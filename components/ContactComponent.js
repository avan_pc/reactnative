import React, { Component } from 'react';
import Card from "react-native-elements/dist/card/Card";
import {Text, ScrollView} from 'react-native';
import * as Animatable from 'react-native-animatable';

class contactUS extends Component {
    render() {
        return(
            <ScrollView>
                <Animatable.View animation='fadeInDown' duration={2000} delay={1000}>
                    <Card>
                        <Card.Title>Contact Information</Card.Title>
                        <Card.Divider />
                        <Text>121, Clear Water Bay Road {"\n"}
                            {"\n"}
                            Clear Water Bay, Kowloon {"\n"}
                            {"\n"}
                            HONG KONG{"\n"}
                            {"\n"}
                            Tel: +852 1234 5678 {"\n"}
                            {"\n"}
                            Fax: +852 8765 4321 {"\n"}
                            {"\n"}
                            Email:confusion@food.net</Text>
                    </Card>
                </Animatable.View>
            </ScrollView>
        );
    }
}

export default contactUS;