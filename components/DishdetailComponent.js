import React, { Component, useState } from 'react';
import {Text, View, FlatList, ScrollView, Button, Modal, StyleSheet, Alert, PanResponder} from 'react-native';
import { Card, Icon, Rating, Input } from 'react-native-elements';
import { connect } from 'react-redux';
import { baseUrl } from '../shared/baseUrl';
import {postFavorite, postComment} from "../redux/ActionCreators";
import * as Animatable from 'react-native-animatable';

const mapStateToProps = state => {
    console.log(state);
    return {
        dishes: state.dishes,
        comments: state.comments,
        favorites: state.favorites
    }
};

const mapDispatchToProps = dispatch => ({
    postFavorite: (dishId) => dispatch(postFavorite(dishId)),
    postComment: (dishId, rating, author, comment) => dispatch(postComment(dishId, rating, author, comment))
});

function RenderComments(props) {

    const comments = props.comments;
    const renderCommentItem = ({item, index}) => {

        return (
            <View key={index} style={{margin: 10}}>
                <Text style={{fontSize: 14}}>{item.comment}</Text>
                <Text style={{fontSize: 12}}><Rating imageSize={20} readonly startingValue={item.rating}/> </Text>
                <Text style={{fontSize: 12}}>{'-- ' + item.author + ', ' + item.date} </Text>
            </View>
        );
    };
    return (
        <Animatable.View animation="fadeInUp" duration={2000} delay={1000}>
            <Card>
            <Card.Title>Comments</Card.Title>
                <Card.Divider/>
                <FlatList
                    data={comments}
                    renderItem={renderCommentItem}
                    keyExtractor={item => item.id.toString()}
                />
            </Card>
        </Animatable.View>
    );
}

function RenderDish(props) {

    const dish = props.dish;
    const [showModal, setShowModal] = useState(false);
    const [comment, setComment] = useState("no input");
    const [author, setAuthor] = useState("no input");
    const [rating,setRating] = useState(0);
    const toggleModal = previous => !previous;

    const recognizeDrag = ({ moveX, moveY, dx }) => {
        if ( dx < -200 )
            return true;
        else if(dx>200)
            return(setShowModal(toggleModal));
        else
            return false
    };
    function handleViewRef(ref) {
        setView(ref)
    }
    const [view, setView] = useState({});

    const panResponder = PanResponder.create({
        onStartShouldSetPanResponder: (e, gestureState) => {
            return true;
        },
        onPanResponderGrant: () => {view.rubberBand(1000).then(endState => console.log(endState.finished ? 'finished' : 'cancelled'));},

        onPanResponderEnd: (e, gestureState) => {
            console.log("pan responder end", gestureState);
            if (recognizeDrag(gestureState))
                Alert.alert(
                    'Add Favorite',
                    'Are you sure you wish to add ' + dish.name + ' to favorite?',
                    [
                        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                        {
                            text: 'OK', onPress: () => {
                                props.favorite ? console.log('Already favorite') : props.onPress()
                            }
                        },
                    ],
                    {cancelable: false}
                );

            return true;
        }
    });

    if (dish != null) {
        return (
            <View>
                <Animatable.View animation="fadeInDown" duration={2000} delay={1000}
                                 ref={handleViewRef}
                                 {...panResponder.panHandlers}>
                    <Card>
                        <Card.Title>{dish.name}</Card.Title>
                        <Card.Divider/>
                        <Card.Image source={{uri: baseUrl + dish.image}}>
                        </Card.Image>
                            <Text style={{margin: 10}}>
                                {dish.description}
                            </Text>
                        <View style={styles.formRow}>
                            <Icon
                                raised
                                reverse
                                name={ props.favorite ? 'heart' : 'heart-o'}
                                type='font-awesome'
                                color='#f50'
                                onPress={() => props.favorite ? console.log('Already favorite') : props.onPress()}
                            />
                            <Icon
                                raised
                                reverse
                                name='pencil'
                                type='font-awesome'
                                color='#f50'
                                onPress={() => setShowModal(toggleModal)}
                            />
                        </View>
                    </Card>
                </Animatable.View>
                <Modal animationType = {"slide"} transparent = {false}
                       visible = {showModal}
                       onDismiss = {() => setShowModal(toggleModal) }
                       onRequestClose = {() => setShowModal(toggleModal) }>
                    <View style = {styles.modal}>
                        <Rating showRating onFinishRating={setRating}/>
                        <Input onChangeText={value=> setAuthor(value)} placeholder='Enter your name' leftIcon={<Icon name='contacts' size={24} color='black'/>} />
                        <Input onChangeText={value=> setComment(value)} placeholder='Enter your comment' leftIcon={<Icon name='create' size={24} color='black'/>} />
                        <View style={styles.formRow}>
                            <Button
                                onPress = {() =>{props.newComment(dish.id, rating, author, comment);
                                    setShowModal(toggleModal);
                                console.log(dish.id, comment, author, rating);}}
                                color="#512DA8"
                                title="SUBMIT"
                            />
                        </View></View>
                        <View style={styles.formRow}>
                            <Button
                                style={styles.formRow}
                                onPress = {() =>{setShowModal(toggleModal)}}
                                color="rgb(50,115,170)"
                                title="CANCEL"
                            />
                        </View>
                </Modal>
            </View>
        )
    }
    else {
        return (<View></View>)
    }
}
class Dishdetail extends Component {
    constructor(props){
        super(props);
        this.newComment=this.newComment.bind(this)
    }

    markFavorite(dishId) {
        this.props.postFavorite(dishId);
    };
    newComment(dishId, rating, author, comment) {
        console.log(author, rating, dishId, comment);
        this.props.postComment(dishId, rating, author, comment);
    };

    static navigationOptions = {
        title: 'Dish Details'
    };
    render() {
        const dishId = this.props.route.params;
        return(
            <ScrollView>
                <RenderDish dish={this.props.dishes.dishes[dishId.dishId]}
                favorite={this.props.favorites.some(el => el === dishId.dishId)}
                onPress={() => this.markFavorite(dishId.dishId)} newComment={this.newComment}/>
                <RenderComments comments={this.props.comments.comments.filter((comment) => comment.dishId === dishId.dishId )}/>
            </ScrollView>
        )
    }

}
const styles = StyleSheet.create({
    formRow: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'row',
        margin: 30
    },
    modal: {
        justifyContent: 'center',
        margin: 20
    },
    modalTitle:{
        fontSize: 24,
        fontWeight: 'bold',
        backgroundColor: '#512DA8',
        textAlign: 'center',
        color: 'white'
    },
    modalText: {
        fontSize: 18,
        margin: 10
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Dishdetail);