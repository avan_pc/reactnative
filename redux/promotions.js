import * as ActionTypes from './ActionTypes';

export const promotions = (state = {
    isLoading: true,
    errMess: null,
    promotions: []
}, action) => {

    switch (action.type) {
        case ActionTypes.ADD_PROMOS:
            return {...state, isLoading: false, errorMess: null, promotions: action.payload};
        case ActionTypes.DISHES_LOADING:
            return {...state, isLoading: true, errMess: null, promotions: []};
        case ActionTypes.DISHES_FAILED:
            return {...state, isLoading: false, errMess: action.payload, promotions: []};
        default:
            return state;
    }
};